from typing import Optional, List
import subprocess
import logging

logger_format = "+++ [%(levelname)s] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=logger_format)


def download(url: str, filename: Optional[str] = None, *, overwrite: bool = False):
    logging.info(f"正在下載 {url}")
    # 如果缺少filename 自動從url中獲取
    # wget需要明確指示`-O`才能覆蓋已存在檔案
    if overwrite:
        filename_opt = f"-O {filename}" if filename else f'-O {url.split("/")[-1]}'
    else:
        filename_opt = ""

    p = subprocess.run(f"wget {filename_opt} {url}", shell=True)
    p.check_returncode()


def preprocess():
    """通用預處理"""
    check_root()
    check_system_requirements()
    check_docker()


def check_root():
    logging.info("嘗試獲得root權限")
    p = subprocess.run("sudo echo 'ok.'", shell=True)
    p.check_returncode()


def check_system_requirements():
    import shutil

    logging.info("檢查系統需求")

    need_depends = ["wget"]
    depends: List[str] = []
    for need_depend in need_depends:
        if shutil.which(need_depend) is None:
            depends.append(need_depend)

    if len(depends) > 0:
        logging.info(f"安裝 > {depends}")
        p = subprocess.run("sudo apt update", shell=True)
        p.check_returncode()
        p = subprocess.run(f"sudo apt install -y {' '.join(depends)}", shell=True)
        p.check_returncode()


def check_docker():
    logging.info("檢查docker安裝")
    r = subprocess.run("sudo docker -v", shell=True)
    if r.returncode != 0:
        logging.info("安裝docker")
        r = subprocess.run("sudo curl -fsSL https://get.docker.com | sh", shell=True)


def checkout_workdir():
    import os
    from pathlib import Path

    workdir = Path.home() / "ant_media_server"
    logging.info(f"跳轉工作目錄 ({workdir})")

    if not workdir.exists():
        workdir.mkdir(exist_ok=True)

    os.chdir(workdir)

    print(os.getcwd())


def start_server():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    p = subprocess.run("sudo docker compose up -d", shell=True)
    p.check_returncode()
    logging.info("已完成")


def restart_server():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    p = subprocess.run("sudo docker compose restart", shell=True)
    p.check_returncode()
    logging.info("已完成")


def stop_server():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    p = subprocess.run("sudo docker compose stop", shell=True)
    p.check_returncode()
    logging.info("已完成")


def uninstall():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    p = subprocess.run("sudo docker compose down", shell=True)
    p.check_returncode()
    logging.info("已完成")


def install_2_3():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.3/ant-media-server-enterprise-2.3.3.1-20210609_2037.zip",
    )
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.3/docker-compose.yml",
        overwrite=True,
    )
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.3/Dockerfile",
        overwrite=True,
    )
    p = subprocess.run("sudo docker compose build", shell=True)
    p.check_returncode()
    logging.info("已完成")


def install_2_4():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.4/ant-media-server-enterprise-2.4.3-20220418_2241.zip",
    )
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.4/docker-compose.yml",
        overwrite=True,
    )
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.4/Dockerfile",
        overwrite=True,
    )
    p = subprocess.run("sudo docker compose build", shell=True)
    p.check_returncode()
    logging.info("已完成")


def install_2_11_3():
    preprocess()
    checkout_workdir()
    logging.info("正在執行")
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.11.3/ant-media-server-enterprise-2.11.3-20240906_0602.zip",
    )
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.11.3/docker-compose.yml",
        overwrite=True,
    )
    download(
        "https://gitlab.com/tta899/ant-media-server/-/raw/master/2.11.3/Dockerfile",
        overwrite=True,
    )
    p = subprocess.run("sudo docker compose build", shell=True)
    p.check_returncode()
    logging.info("已完成")


def menu():
    tasks = {
        "1": {"name": "啟動server", "action": start_server},
        "2": {
            "name": "重啟server",
            "action": restart_server,
        },
        "3": {
            "name": "停止server",
            "action": stop_server,
        },
        "4": {
            "name": "解除安裝",
            "action": uninstall,
        },
        "2.3": {
            "name": "安裝2.3版本",
            "action": install_2_3,
        },
        "2.4": {
            "name": "安裝2.4版本",
            "action": install_2_4,
        },
        "2.11.3": {
            "name": "安裝2.11.3版本",
            "action": install_2_11_3,
        },
        "q": {
            "name": "退出",
            "action": lambda: exit(0),
        },
    }

    for i in tasks:
        print(f"({i}) {tasks[i]['name']}")

    print("")  # 空行
    while True:
        i = input("請輸入選項: ").strip()
        if i in tasks:
            t = tasks[i]
            break

    logging.info(t["name"])
    t["action"]()


menu()
